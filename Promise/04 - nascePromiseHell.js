const axios = require('axios');

let usuario;
let usuario2;
let usuario3

axios.get('https://jsonplaceholder.typicode.com/todos/1').then((resposta) => {
  usuario = resposta.data;

  axios.get('https://jsonplaceholder.typicode.com/todos/2').then((resposta) => {
    usuario2 = resposta.data;
    
    axios.get('https://jsonplaceholder.typicode.com/todos/3').then((resposta) => {
      usuario3 = resposta.data;

      console.log('Usuário 1', usuario);
      console.log('Usuário 2', usuario2);
      console.log('Usuário 3', usuario3);
    });
  });
});
