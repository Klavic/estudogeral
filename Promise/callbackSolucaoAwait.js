(async () => {

  const jwt = require('jsonwebtoken');

  const token = {
    nome: 'Tiago Felício Castro da Silveira',
    idade: 27,
  };

  function criptografaToken() {
    return new Promise((resolve, reject) => {
      jwt.sign(token, 'senha123', function (err, data) {
        console.log('');
        console.log('Sucesso');
        console.log('Erro se houver:', err);
        console.log('Token gerado dentro da Promise:', data);

        if (!!err === true) {
          return reject(err);
        }

        return resolve(data);
      });
    });
  }

  const criptografaTokenAsync = async () => {
    jwt.sign(token, 'senha123', function (err, data) {
      console.log('');
      console.log('Sucesso');
      console.log('Erro se houver:', err);
      console.log('Token gerado dentro da Promise:', data);

      if (!!err === true) {
        return Promise.reject(err);
      }

      return Promise.resolve(data);
    });
  }

  try {
    const tokenEncoded = await criptografaTokenAsync();
    console.log('');
    console.log('await :', tokenEncoded);
  } catch (err) {
    console.log(err);
  }

})();
