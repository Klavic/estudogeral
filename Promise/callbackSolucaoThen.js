const jwt = require('jsonwebtoken');

const token = {
  nome: 'Tiago Felício Castro da Silveira',
  idade: 27,
};

function criptografaToken() {
  return new Promise((resolve, reject) => {
    jwt.sign(token, 'senha123', function (err, data) {
      console.log('');
      console.log('Sucesso');
      console.log('Erro se houver:', err);
      console.log('Token gerado dentro da Promise:', data);

      if (!!err === true) {
        return reject(err);
      }

      return resolve(data);
    });
  });
}

criptografaToken().then((tokenEncoded) => {
  console.log('')
  console.log('.then(): ', tokenEncoded);
}).then(() => {
  console.log('ola');
  throw('erro qualquer')
}).catch((err) => {
  console.log('Erro', err);
});
