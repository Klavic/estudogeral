const axios = require('axios');

let usuario;
let usuario2;
let usuario3


axios.get('https://jsonplaceholder.typicode.com/todos/1').then((resposta) => {
  usuario = resposta.data;

  return axios.get('https://jsonplaceholder.typicode.com/todos/2');
}).then((dadosUsuario2) => {
  usuario2 = dadosUsuario2.data;

  return axios.get('https://jsonplaceholder.typicode.com/todos/3');
}).then((dadosUsuario3) => {
  usuario3 = dadosUsuario3.data;

  console.log('Usuário 1', usuario);
  console.log('Usuário 2', usuario2);
  console.log('Usuário 3', usuario3);
});