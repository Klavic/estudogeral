const axios = require('axios');

let usuario;
let usuario2;
let usuario3;

axios.get('https://jsonplaceholder.typicode.com/todos/1')
  .then((resposta) => {
    usuario = resposta.data;
    console.log('Then', usuario);
  });

console.log('Fora do Then', usuario);

axios.get('https://jsonplaceholder.typicode.com/todos/2')
  .then((resposta) => {
    usuario2 = resposta.data;
    console.log('Then', usuario2);
  });

console.log('Fora do Then 2', usuario2);

axios.get('https://jsonplaceholder.typicode.com/todos/3')
  .then((resposta) => {
    usuario3 = resposta.data;
    console.log('Then', usuario3);
  });

console.log('Fora do Then 2', usuario3);