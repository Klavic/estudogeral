const axios = require('axios');

(async () => {

  const usuario1 = await axios.get('https://jsonplaceholder.typicode.com/todos/1');
  console.log('Usuário 1', usuario1.data);

  const usuario2 = await axios.get('https://jsonplaceholder.typicode.com/todos/2');
  console.log('Usuário 2', usuario2.data);

  const usuario3 = await axios.get('https://jsonplaceholder.typicode.com/todos/3');
  console.log('Usuário 3', usuario3.data);

})();

async function testeAsync() {
  const usuario3 = await axios.get('https://jsonplaceholder.typicode.com/todos/3');
}

const funcaoAsync2 = async () => {
  const usuario3 = await axios.get('https://jsonplaceholder.typicode.com/todos/3');
}