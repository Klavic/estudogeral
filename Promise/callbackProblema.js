const jwt = require('jsonwebtoken');

const token = {
  nome: 'Tiago Felício Castro da Silveira',
  idade: 27,
};

/**
 * Callback sucesso.
 */
const tokenEncoded1 = jwt.sign(token, 'senha123', function (err, data) {
  console.log('');
  console.log('Sucesso');
  console.log('Erro se houver:', err);
  console.log('Token gerado dentro da Promise:', data);
});

console.log('Apos a promise tokenEncoded1: ', tokenEncoded1);

/**
 * Callback com erro.
 */
const tokenEncoded2 = jwt.sign(undefined, 'senha123', function (err, data) {
  console.log('');
  console.log('Erro');
  console.log('Erro se houver:', err);
  console.log('Token gerado dentro da Promise:', data);
});

console.log('Apos a promise tokenEncoded2: ', tokenEncoded2);