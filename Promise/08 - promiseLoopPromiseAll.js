const axios = require('axios');


async function funcaoAssincronaAsync(posicao) {
  const dados = await axios.get(`https://jsonplaceholder.typicode.com/todos/${posicao}`);
  console.log(dados.data.id);
  return Promise.resolve(dados.dados);
}


const list = [1, 2, 3, 4, 5];

// for (let i = 0; i < list.length; i += 1) {
//   funcaoAssincronaAsync(list[i]).then((retorno) => {
//     console.log(`Fim ${i}`);
//   });
// }
const listaPromise = [];

// list.forEach((value) => {
//   listaPromise.push(funcaoAssincronaAsync(value).then((retorno) => {
//     console.log(`Fim ${value}`);
//   }));
// });

list.reduce(
  (promise, value) => promise.then(() => funcaoAssincronaAsync(value)),
  Promise.resolve([])
);
// list.reduce(funcaoAssincronaAsync, Promise.resolve());
// Promise.mapSeries(list, funcaoAssincronaAsync)