const axios = require('axios');



function funcaoAssincrona(posicao) {
  return new Promise((resolve, reject) => {
    axios.get('https://jsonplaceholder.typicode.com/todos/1').then((dados) => {
      console.log(dados.data);
      return resolve(dados.dados);
    });
  });
}


async function funcaoAssincronaAsync(posicao) {
  const dados = await axios.get('https://jsonplaceholder.typicode.com/todos/2');
  console.log(dados.data);
  return Promise.resolve(dados.dados);
}

funcaoAssincrona(1).then(() => {
  console.log('ola');
})
funcaoAssincronaAsync(1).then(() => {
  console.log('oi')
});
