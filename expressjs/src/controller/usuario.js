const listaUsuario = [];

module.exports = () => {
  const controller = {};

  controller.adicionaUsuario = (req, res) => {
    const usuario = req.body.usuario;

    listaUsuario.push(usuario);
    return res.status(200).json({ mensagem: 'Usuário inserido com sucesso.' });
  };

  controller.listaUsuario = (req, res) => {
    return res.status(200).json({ mensagem: 'sucesso', dados: listaUsuario });
  };

  controller.buscaUsuario = (req, res) => {
    const idUsuario = req.params.idUsuario;

    let usuario
    listaUsuario.forEach((pUsuario) => {
      if (pUsuario.id === Number(idUsuario)) {
        usuario = pUsuario;
      }
    });

    if (!!usuario === false) {
      return res.status(404).json({ mensagem: 'Usuário não encontrado' });
    }
    return res.status(200).json({ mensagem: 'Sucesso', dados: usuario });
  }

  return controller;
}
