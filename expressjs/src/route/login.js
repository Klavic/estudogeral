const controller = require('../controller/login')();

module.exports = (app) => {
  app.get('/login', controller.login);

  app.get('/helloworld', controller.helloWorld);
}
