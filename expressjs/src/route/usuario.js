const controller = require('../controller/usuario')();
const seguranca = require('../midelware/seguranca')();

module.exports = (app) => {

  app.get('/usuario', controller.listaUsuario);
  app.get('/usuario/:idUsuario', controller.buscaUsuario);
  app.post('/usuario', seguranca.validarLogado, seguranca.permitePerfil('admin'), controller.adicionaUsuario);

}