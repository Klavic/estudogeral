module.exports = () => {
  const protetor = {};

  protetor.validarLogado = (req, res, next) => {
    const token = req.headers.authorization;

    if (!!token === false) {
      return res.status(403).json({ mensagem: 'Não autenticado' });
    }

    return next();
  }
  protetor.permitePerfil = (pTipoUsuario) => {
    return (req, res, next) => {
      const token = req.headers.authorization;

      if (token !== pTipoUsuario) {
        return res.status(401).json({ mensagem: 'Não autorizado' });
      }

      return next();
    }
  }

  return protetor;
}