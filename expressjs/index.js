const http = require('http');
const Express = require('express');
const bodyParse = require('body-parser');


const env = require('./src/config/env');
const app = Express();

app.use((req, res, next) => {
  res.header('Content-Type', 'application/json');
  next();
});

app.use(bodyParse.json());

// Rotas
require('./src/route/usuario')(app);
require('./src/route/login')(app);

const servidor = http.createServer(app);

servidor.listen(env.porta, () => {
  console.log(`Servidor rodano na porta: ${env.porta}`);
});
