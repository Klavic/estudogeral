function talk() {
  console.log(this.sound);
};

talk(); // undefined

const boromir = {
  sound: 'On does not simple walk into mordor',
};

const talkBoundToBoromir = talk.bind(boromir);
talkBoundToBoromir(); // 'On does not simple walk into mordor'

const boromir2 = {
  speak: talk, // A função foi atribuida dentro do objeto assim fazendo o vinculo ao objeto.
  sound: 'On does not simple walk into mordor',
};
boromir2.speak(); // 'On does not simple walk into mordor'

boromir2.speak2 = talk.bind(boromir2);
boromir2.speak2(); // 'On does not simple walk into mordor'