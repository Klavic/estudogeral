function Person(saying) {
  this.saying = saying;
}

Person.prototype.talk = function () {
  console.log(`I say: ${this.saying}`);
};

const crockforg = new Person('SEMICOLANS!!!1one1');

crockforg.talk();


function newDo(constructor, args) {
  /**
   * Essa função simula o que a função faz ao criar um novo Objeto / Class.
   */

  const obj = {};
  Object.setPrototypeOf(obj, constructor.prototype);
  const argsArray = Array.from(arguments);
  constructor.apply(obj, argsArray.slice(1));
  return obj;
}

const crockforg2 = newDo(Person, 'SEMICOLANS!!!1one1');
crockforg2.talk();