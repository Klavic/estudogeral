const dog = {
  sound: 'woof',
  talk: function () { console.log(this.sound) },
};

const cat = {
  sound: 'Miau',
};


dog.talk(); // 'woof'


/**
 * Quando a função talk foi atribuida a variavel ele somente atribuiu a função com isso perdendo a referencia do this.
 */
const talkFunction = dog.talk;
talkFunction(); // undefined

/**
 * Usando o bind e feito o vinculo da função a um  objeto retornando a referencia ao this do objeto.
 */
const boundFunction = talkFunction.bind(dog);
boundFunction(); // 'woof'

const boundFunction2 = talkFunction.bind(cat);
boundFunction2(); // 'Miau'


function walkAndTalk() {
  console.log(`It walk and ${this.sound}`);
}

const boundFunctionCat = walkAndTalk.bind(cat);
boundFunctionCat(); // It walk and Miau