function talk(sound) {
  console.log(sound);
};
talk('woof'); // 'woof'

function talk2() {
  console.log(this.sound);
}
talk2(); // undefined

const animal = {
  talk: talk2,
};

const cat = {
  sound: 'meow!',
};

const dog = {
  sound: 'woof!',
};

const prarieDog = {
  howl: function () {
    console.log(this.sound.toUpperCase());
  }
}

Object.setPrototypeOf(cat, animal);
cat.talk(); // 'meow!'
Object.setPrototypeOf(dog, animal);
dog.talk(); // 'woof!'

Object.setPrototypeOf(prarieDog,dog);
prarieDog.howl(); // 'WOOF!'
